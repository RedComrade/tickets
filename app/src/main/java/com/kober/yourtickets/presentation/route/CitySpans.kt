package com.kober.yourtickets.presentation.route

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.widget.EditText
import androidx.core.content.ContextCompat
import com.kober.yourtickets.R
import com.kober.yourtickets.extencions.setTextIgnoreWatcher


fun setCitySpanned(context: Context, text: String, editText: EditText, textWatcher: TextWatcher) {
    val spanString = SpannableString(text).apply {
        setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorSpecialAccent)),
            0, text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        setSpan(StyleSpan(Typeface.BOLD), 0, text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
    editText.setTextIgnoreWatcher(spanString, textWatcher)
}

fun clearCitySpans(editText: EditText) {
    val text = editText.text ?: return
    text.getSpans(0, text.length, ForegroundColorSpan::class.java).getOrNull(0)?.let {
        text.removeSpan(it)
    }
    text.getSpans(0, text.length, StyleSpan::class.java).getOrNull(0)?.let {
        text.removeSpan(it)
    }
}