package com.kober.yourtickets.presentation.route.list

import android.content.Context
import androidx.annotation.ColorRes
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.kober.yourtickets.domain.dto.route.CityDto


class CityAdapter(
    context: Context, @ColorRes cityPatternColor: Int, onItemClick: (CityDto) -> Unit
) : ListDelegationAdapter<List<CityDto>>() {
    private var cityAdapterDelegate: CityAdapterDelegate

    init {
        items = mutableListOf()
        delegatesManager.apply {
            cityAdapterDelegate = CityAdapterDelegate(context, cityPatternColor, onItemClick)
            addDelegate(cityAdapterDelegate)
        }
    }

}