package com.kober.yourtickets.presentation.route.arrival

import com.arellomobile.mvp.InjectViewState
import com.kober.yourtickets.domain.dto.route.CityDto
import com.kober.yourtickets.domain.interactor.route.ChooseCityInteractor
import com.kober.yourtickets.domain.state.route.ChooseRouteState
import com.kober.yourtickets.domain.state.route.CitiesListState
import com.kober.yourtickets.presentation.BasePresenter
import javax.inject.Inject


@InjectViewState
class ChooseArrivalPresenter @Inject constructor(
    private val routeState: ChooseRouteState,
    private val citiesListState: CitiesListState,
    private val chooseCityInteractor: ChooseCityInteractor
) : BasePresenter<ChooseArrivalView>() {

    override fun onFirstViewAttach() {
        untilDestroy(
            chooseCityInteractor.observeCitiesSuggestions(),
            citiesListState.loading.subscribeOnMain(viewState::showLoading),
            citiesListState.listUpdate.subscribeOnMain(viewState::showSuggestions)
        )
    }

    fun onCitySearchTextChanged(searchText: String) {
        chooseCityInteractor.requestCitiesSuggestions(searchText)
    }

    fun chooseArrivalCity(city: CityDto) {
        routeState.chosenArrivalCity = city
    }

    fun dropArrivalCity() {
        routeState.chosenArrivalCity = null
    }
}