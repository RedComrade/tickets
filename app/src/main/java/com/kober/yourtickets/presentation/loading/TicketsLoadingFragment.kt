package com.kober.yourtickets.presentation.loading

import android.animation.FloatArrayEvaluator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.kober.yourtickets.R
import com.kober.yourtickets.di.DI
import com.kober.yourtickets.di.close
import com.kober.yourtickets.di.module.TicketsLoadingModule
import com.kober.yourtickets.domain.dto.route.CityDto
import com.kober.yourtickets.domain.dto.route.RouteCitiesDto
import com.kober.yourtickets.extencions.argument
import com.kober.yourtickets.extencions.getBearing
import com.kober.yourtickets.extencions.getBezierPoints
import com.kober.yourtickets.extencions.getBitmapForLayout
import com.kober.yourtickets.presentation.BaseFragment
import timber.log.Timber


class TicketsLoadingFragment : BaseFragment(), TicketsLoadingView, OnMapReadyCallback {
    override val layoutRes = R.layout.fragment_tickets_loading

    @InjectPresenter
    internal lateinit var presenter: TicketsLoadingPresenter

    @ProvidePresenter
    fun providePresenter(): TicketsLoadingPresenter =
        DI.ticketsLoadingScope.getInstance(TicketsLoadingPresenter::class.java)

    private val routeCitiesDto by argument<RouteCitiesDto>(ROUTE_KEY)
    private var latLngAnimator: ValueAnimator? = null
    private var map: GoogleMap? = null
    private lateinit var planeMarker: Marker
    private var animTimePerStep = 0L
    private var routePoints = emptyList<LatLng>()
    override fun initScope() = DI.ticketsLoadingScope.installModules(TicketsLoadingModule(routeCitiesDto))
    override fun destroyScope() = DI.ticketsLoadingScope.close()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val fragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        fragment.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap) {
        map.uiSettings.isRotateGesturesEnabled = false
        this.map = map
        presenter.onMapReady()
    }

    override fun initRoute(routeCities: RouteCitiesDto, loadingProgress: Long, remainingTimeMs: Long) {
        map?.let { map ->
            val (departure, arrival) = routeCities
            addCityMarker(map, departure)
            addCityMarker(map, arrival)
            val fromPoint = LatLng(departure.latitude, departure.longitude)
            val toPoint = LatLng(arrival.latitude, arrival.longitude)
            routePoints = getBezierPoints(fromPoint, toPoint, ROUTE_POINTS_COUNT)
            drawRoute(map, routePoints)
            moveCamera(map, departure, arrival)

            val planeStartPointIndex = (if (loadingProgress > 0) loadingProgress - 1 else 0).toInt()
            planeMarker = initPlaneMarker(map, routePoints[planeStartPointIndex], routePoints[planeStartPointIndex + 1])

            // disregard that distance between points could be slightly different
            val remainingSteps = routePoints.size - loadingProgress
            animTimePerStep = (remainingTimeMs / remainingSteps)
            Timber.d("animTimePerStep: $animTimePerStep")
        }
    }

    override fun startPlaneAnimation(loadingIndex: Long) {
        if (map == null) return
        latLngAnimator?.removeAllListeners()
        latLngAnimator?.cancel()

        val startPos = routePoints[loadingIndex.toInt()]
        val destPos = routePoints[loadingIndex.toInt() + 1]
        val startValues = floatArrayOf(startPos.latitude.toFloat(), startPos.longitude.toFloat())
        val endValues = floatArrayOf(destPos.latitude.toFloat(), destPos.longitude.toFloat())
        latLngAnimator = ValueAnimator.ofObject(FloatArrayEvaluator(), startValues, endValues)
            .also { animator ->
                animator.duration = animTimePerStep
                animator.interpolator = LinearInterpolator()
                animator.addUpdateListener { animation ->
                    val animatedValue = animation.animatedValue as FloatArray
                    val oldTickPos = planeMarker.position
                    val newTickPos = LatLng(animatedValue[0].toDouble(), animatedValue[1].toDouble())
                    planeMarker.position = newTickPos

                    val rotation = getBearing(oldTickPos, newTickPos, PLANE_SOURCE_ANGLE).toFloat()
                    if (rotation != 0f && rotation != -PLANE_SOURCE_ANGLE) planeMarker.rotation = rotation
                }
                animator.start()
            }
    }

    override fun interceptBackPressed(): Boolean {
        presenter.back()
        return true
    }

    private fun moveCamera(map: GoogleMap, from: CityDto, to: CityDto) {
        val bounds = LatLngBounds.Builder().apply {
            include(LatLng(from.latitude, from.longitude))
            include(LatLng(to.latitude, to.longitude))
        }.build()
        val width = resources.displayMetrics.widthPixels
        val padding = (width * 0.10).toInt()
        // todo: setup max zoom: sometimes it's not enough
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding))
    }

    private fun drawRoute(map: GoogleMap, routePoints: List<LatLng>) {
        val polylineOptions =
            PolylineOptions()
                .jointType(JointType.ROUND)
                .pattern(listOf(Dot(), Gap(20f)))
                .color(ContextCompat.getColor(context, R.color.colorAccent))
                .addAll(routePoints)
        map.addPolyline(polylineOptions)
    }

    private fun addCityMarker(map: GoogleMap, city: CityDto) {
        val pinText = if (city.code.isEmpty()) "${city.id}?" else city.code
        val pinBitmap = context.getBitmapForLayout(requireNotNull(view), R.layout.pin_city) {
            (it as TextView).text = pinText
        }
        map.addMarker(
            MarkerOptions()
                .position(LatLng(city.latitude, city.longitude))
                .anchor(0.5f, 0.5f)
                .alpha(0.7f)
                .icon(BitmapDescriptorFactory.fromBitmap(pinBitmap))
        )
    }

    private fun initPlaneMarker(map: GoogleMap, firstPoint: LatLng, secondPoint: LatLng): Marker {
        val planeBitmap = context.getBitmapForLayout(requireNotNull(view), R.layout.pin_plane) {}
        return map.addMarker(
            MarkerOptions()
                .position(firstPoint)
                .anchor(0.5f, 0.5f)
                .zIndex(10f)
                .rotation(getBearing(firstPoint, secondPoint, PLANE_SOURCE_ANGLE).toFloat())
                .icon(BitmapDescriptorFactory.fromBitmap(planeBitmap))
        )
    }

    companion object {
        const val ROUTE_POINTS_COUNT = 150L
        private const val PLANE_SOURCE_ANGLE = 90f
        private const val ROUTE_KEY = "route_key"
        fun newInstance(routeCitiesDto: RouteCitiesDto) = TicketsLoadingFragment().apply {
            // todo use parcelable -_-
            arguments = Bundle().apply { putSerializable(ROUTE_KEY, routeCitiesDto) }
        }
    }
}