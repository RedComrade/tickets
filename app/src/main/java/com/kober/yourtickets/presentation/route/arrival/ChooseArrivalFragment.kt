package com.kober.yourtickets.presentation.route.arrival

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kober.yourtickets.R
import com.kober.yourtickets.di.DI
import com.kober.yourtickets.di.close
import com.kober.yourtickets.di.module.ArrivalModule
import com.kober.yourtickets.domain.dto.route.CityListUpdate
import com.kober.yourtickets.extencions.dimen
import com.kober.yourtickets.extencions.getSimpleTextWatcher
import com.kober.yourtickets.extencions.updateItems
import com.kober.yourtickets.extencions.visibleOrGone
import com.kober.yourtickets.presentation.BaseFragment
import com.kober.yourtickets.presentation.route.clearCitySpans
import com.kober.yourtickets.presentation.route.list.CityAdapter
import com.kober.yourtickets.presentation.route.setCitySpanned
import com.kober.yourtickets.presentation.widget.MarginItemDecoration
import kotlinx.android.synthetic.main.fragment_choose_arrival.*


class ChooseArrivalFragment : BaseFragment(), ChooseArrivalView {
    override val layoutRes = R.layout.fragment_choose_arrival

    @InjectPresenter
    internal lateinit var presenter: ChooseArrivalPresenter

    @ProvidePresenter
    fun providePresenter(): ChooseArrivalPresenter = DI.arrivalScope.getInstance(ChooseArrivalPresenter::class.java)

    override fun initScope() = DI.arrivalScope.installModules(ArrivalModule())
    override fun destroyScope() = DI.arrivalScope.close()


    private lateinit var arrivalCitiesAdapter: CityAdapter

    private val textWatcher = getSimpleTextWatcher {
        clearCitySpans(citiesArrivalEditText)
        presenter.onCitySearchTextChanged(it.toString())
        presenter.dropArrivalCity()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arrivalCitiesAdapter = CityAdapter(context, R.color.colorAccent) {
            setCitySpanned(context, it.fullName, citiesArrivalEditText, textWatcher)
            presenter.chooseArrivalCity(it)
        }

        arrivalCitiesRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = arrivalCitiesAdapter
            val topMargin = context.dimen(R.dimen.city_item_margin_top).toInt()
            val sideMargin = context.dimen(R.dimen.city_item_margin_side).toInt()
            addItemDecoration(MarginItemDecoration(topMargin, sideMargin))
        }
    }

    override fun onStart() {
        super.onStart()
        citiesArrivalEditText.addTextChangedListener(textWatcher)
    }

    override fun onStop() {
        citiesArrivalEditText.removeTextChangedListener(textWatcher)
        super.onStop()
    }

    override fun showSuggestions(listUpdate: CityListUpdate) {
        val isInRestoreState = presenter.isInRestoreState(this)
        arrivalCitiesAdapter.updateItems(listUpdate, !isInRestoreState)
        val nothingToShow = listUpdate.list.isEmpty()
        val inputIsEmpty = citiesArrivalEditText.editableText.isEmpty()
        arrivalCitiesEmptyHint.visibleOrGone(nothingToShow && inputIsEmpty)
        arrivalCitiesNotFoundHint.visibleOrGone(nothingToShow && !inputIsEmpty)
    }

    override fun showLoading(loading: Boolean) {
        arrivalLoading.visibleOrGone(loading)
    }

    companion object {
        fun newInstance() = ChooseArrivalFragment()
    }

}