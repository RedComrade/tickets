package com.kober.yourtickets.presentation.route.departure

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kober.yourtickets.R
import com.kober.yourtickets.di.DI
import com.kober.yourtickets.di.close
import com.kober.yourtickets.di.module.DepartureModule
import com.kober.yourtickets.domain.dto.route.CityListUpdate
import com.kober.yourtickets.extencions.dimen
import com.kober.yourtickets.extencions.getSimpleTextWatcher
import com.kober.yourtickets.extencions.updateItems
import com.kober.yourtickets.extencions.visibleOrGone
import com.kober.yourtickets.presentation.BaseFragment
import com.kober.yourtickets.presentation.route.clearCitySpans
import com.kober.yourtickets.presentation.route.list.CityAdapter
import com.kober.yourtickets.presentation.route.setCitySpanned
import com.kober.yourtickets.presentation.widget.MarginItemDecoration
import kotlinx.android.synthetic.main.fragment_choose_departure.*


class ChooseDepartureFragment : BaseFragment(), ChooseDepartureView {
    override val layoutRes = R.layout.fragment_choose_departure

    @InjectPresenter
    internal lateinit var presenter: ChooseDeparturePresenter

    @ProvidePresenter
    fun providePresenter(): ChooseDeparturePresenter =
        DI.departureScope.getInstance(ChooseDeparturePresenter::class.java)

    override fun initScope() = DI.departureScope.installModules(DepartureModule())
    override fun destroyScope() = DI.departureScope.close()

    private lateinit var departureCitiesAdapter: CityAdapter
    private val textWatcher = getSimpleTextWatcher {
        clearCitySpans(citiesDepartureEditText)
        presenter.dropDepartureCity()
        presenter.onCitySearchTextChanged(it.toString())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        departureCitiesAdapter = CityAdapter(context, R.color.colorAccent) {
            setCitySpanned(context, it.fullName, citiesDepartureEditText, textWatcher)
            presenter.chooseDepartureCity(it)
        }
        departureRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = departureCitiesAdapter
            val topMargin = context.dimen(R.dimen.city_item_margin_top).toInt()
            val sideMargin = context.dimen(R.dimen.city_item_margin_side).toInt()
            addItemDecoration(MarginItemDecoration(topMargin, sideMargin))
        }
    }

    override fun onStart() {
        super.onStart()
        citiesDepartureEditText.addTextChangedListener(textWatcher)
    }

    override fun onStop() {
        citiesDepartureEditText.removeTextChangedListener(textWatcher)
        super.onStop()
    }

    override fun showSuggestions(listUpdate: CityListUpdate) {
        val isInRestoreState = presenter.isInRestoreState(this)
        departureCitiesAdapter.updateItems(listUpdate, !isInRestoreState)
        val nothingToShow = listUpdate.list.isEmpty()
        val inputIsEmpty = citiesDepartureEditText.editableText.isEmpty()
        departureCitiesEmptyHint.visibleOrGone(nothingToShow && inputIsEmpty)
        departureCitiesNotFoundHint.visibleOrGone(nothingToShow && !inputIsEmpty)
    }

    override fun showLoading(loading: Boolean) {
        departureLoading.visibleOrGone(loading)
    }

    companion object {
        fun newInstance() = ChooseDepartureFragment()
    }
}