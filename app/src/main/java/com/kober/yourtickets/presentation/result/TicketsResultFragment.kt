package com.kober.yourtickets.presentation.result

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kober.yourtickets.R
import com.kober.yourtickets.di.DI
import com.kober.yourtickets.presentation.BaseFragment
import kotlinx.android.synthetic.main.fragment_tickets_result.*


class TicketsResultFragment : BaseFragment(), TicketsResultView {
    override val layoutRes = R.layout.fragment_tickets_result

    @InjectPresenter
    internal lateinit var presenter: TicketsResultPresenter

    @ProvidePresenter
    fun providePresenter(): TicketsResultPresenter =
        DI.appScope.getInstance(TicketsResultPresenter::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ticketsResultRepeat.setOnClickListener { presenter.repeatSearch() }
    }

    override fun interceptBackPressed(): Boolean {
        presenter.back()
        return true
    }

    companion object {
        fun newInstance() = TicketsResultFragment()
    }
}