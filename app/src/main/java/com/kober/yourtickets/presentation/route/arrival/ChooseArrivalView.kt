package com.kober.yourtickets.presentation.route.arrival

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.kober.yourtickets.domain.dto.route.CityListUpdate


interface ChooseArrivalView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showSuggestions(listUpdate: CityListUpdate)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoading(loading: Boolean)

}