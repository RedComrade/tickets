package com.kober.yourtickets.presentation.route.tabs

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kober.yourtickets.R
import com.kober.yourtickets.di.DI
import com.kober.yourtickets.di.close
import com.kober.yourtickets.di.module.RouteModule
import com.kober.yourtickets.extencions.snackBar
import com.kober.yourtickets.presentation.BaseFragment
import kotlinx.android.synthetic.main.fragment_choose_route.*


class ChooseRouteFragment : BaseFragment(), ChooseRouteView {
    override val layoutRes = R.layout.fragment_choose_route

    @InjectPresenter
    internal lateinit var presenter: ChooseRoutePresenter

    @ProvidePresenter
    fun providePresenter(): ChooseRoutePresenter = DI.routeScope.getInstance(ChooseRoutePresenter::class.java)

    override fun initScope() = DI.routeScope.installModules(RouteModule())
    override fun destroyScope() = DI.routeScope.close()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        routeViewpager.adapter = RouteTabsAdapter(context, childFragmentManager)
        routeTabLayout.setupWithViewPager(routeViewpager)
        routeSearch.setOnClickListener { presenter.onSearchClicked() }
    }

    override fun showError(message: String) {
        snackBar(message)
    }

    companion object {
        fun newInstance() = ChooseRouteFragment()
    }

}