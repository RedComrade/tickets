package com.kober.yourtickets.presentation.route.departure

import com.arellomobile.mvp.InjectViewState
import com.kober.yourtickets.domain.dto.route.CityDto
import com.kober.yourtickets.domain.interactor.route.ChooseCityInteractor
import com.kober.yourtickets.domain.state.route.ChooseRouteState
import com.kober.yourtickets.domain.state.route.CitiesListState
import com.kober.yourtickets.presentation.BasePresenter
import javax.inject.Inject


@InjectViewState
class ChooseDeparturePresenter @Inject constructor(
    private val routeState: ChooseRouteState,
    private val citiesListState: CitiesListState,
    private val chooseCityInteractor: ChooseCityInteractor
) : BasePresenter<ChooseDepartureView>() {

    override fun onFirstViewAttach() {
        untilDestroy(
            chooseCityInteractor.observeCitiesSuggestions(),
            citiesListState.loading.subscribeOnMain(viewState::showLoading),
            citiesListState.listUpdate.subscribeOnMain(viewState::showSuggestions)
        )
    }

    fun onCitySearchTextChanged(searchText: String) {
        chooseCityInteractor.requestCitiesSuggestions(searchText)
    }

    fun chooseDepartureCity(city: CityDto) {
        routeState.chosenDepartureCity = city
    }

    fun dropDepartureCity() {
        routeState.chosenDepartureCity = null
    }

}