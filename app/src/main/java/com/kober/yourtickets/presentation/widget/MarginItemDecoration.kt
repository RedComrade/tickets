package com.kober.yourtickets.presentation.widget

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class MarginItemDecoration(
    private val topMarginPx: Int,
    private val sideMarginPx: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val isFirstPosition = parent.getChildAdapterPosition(view) == 0
        outRect.set(sideMarginPx, if (isFirstPosition) topMarginPx else 0, sideMarginPx, 0)
    }

}
