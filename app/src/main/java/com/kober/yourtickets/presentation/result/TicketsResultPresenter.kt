package com.kober.yourtickets.presentation.result

import com.kober.yourtickets.navigation.Screens
import com.kober.yourtickets.presentation.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject


class TicketsResultPresenter @Inject constructor(
    private val router: Router
) : BasePresenter<TicketsResultView>() {

    fun back() {
        router.newRootChain(Screens.ChooseCities)
    }

    fun repeatSearch() {
        router.newRootChain(Screens.ChooseCities)
    }

}