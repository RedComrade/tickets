package com.kober.yourtickets.presentation.route.tabs

import com.arellomobile.mvp.InjectViewState
import com.kober.yourtickets.R
import com.kober.yourtickets.data.system.ResourceManager
import com.kober.yourtickets.domain.state.route.ChooseRouteState
import com.kober.yourtickets.navigation.Screens
import com.kober.yourtickets.presentation.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@InjectViewState
class ChooseRoutePresenter @Inject constructor(
    private val chooseRouteState: ChooseRouteState,
    private val resourceManager: ResourceManager,
    private val router: Router
) : BasePresenter<ChooseRouteView>() {

    override fun onFirstViewAttach() {
        untilDestroy(
            chooseRouteState.error.subscribeOnMain(viewState::showError)
        )
    }

    fun onSearchClicked() {
        chooseRouteState.getRouteIfChosen()
            ?.let {
                if (it.arrivalCity.isSamePlace(it.departureCity)) viewState.showError(resourceManager.getString(R.string.cities_are_equals))
                else router.navigateTo(Screens.TicketsLoading(it))
            }
            ?: viewState.showError(resourceManager.getString(R.string.route_not_chosen))
    }

}