package com.kober.yourtickets.presentation.loading

import com.arellomobile.mvp.InjectViewState
import com.kober.yourtickets.domain.interactor.tickets.TicketsInteractor
import com.kober.yourtickets.domain.state.tickets.TicketsState
import com.kober.yourtickets.navigation.Screens
import com.kober.yourtickets.presentation.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@InjectViewState
class TicketsLoadingPresenter @Inject constructor(
    private val ticketsInteractor: TicketsInteractor,
    private val ticketsState: TicketsState,
    private val router: Router
) : BasePresenter<TicketsLoadingView>() {

    override fun onFirstViewAttach() {
        untilDestroy(
            ticketsState.ticketsLoadingInfo.subscribeOnMain { viewState.startPlaneAnimation(it.loadingIndex) },
            ticketsState.ticketsLoaded.subscribeOnMain { router.newRootScreen(Screens.TicketsResult) },
            ticketsInteractor.getTickets(TicketsLoadingFragment.ROUTE_POINTS_COUNT)
        )
    }

    fun onMapReady() {
        val (loadingProgress, timeRemains) = requireNotNull(ticketsState.ticketsLoadingInfo.value)
        viewState.initRoute(ticketsState.routeCities, loadingProgress, timeRemains)
    }

    fun back() {
        router.exit()
    }

}