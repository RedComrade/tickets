package com.kober.yourtickets.presentation

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber


open class BasePresenter<View : MvpView> : MvpPresenter<View>() {

    private val lifecycleDisposable = CompositeDisposable()

    override fun onDestroy() {
        lifecycleDisposable.dispose()
    }

    fun untilDestroy(vararg disposables: Disposable) {
        for (disposable in disposables) {
            lifecycleDisposable.add(disposable)
        }
    }

    fun Disposable.untilDestroy() {
        lifecycleDisposable.add(this)
    }

    protected fun <T> Observable<T>.subscribeOnMain(onNext: (T) -> Unit): Disposable {
        return observeOn(AndroidSchedulers.mainThread()).subscribe(onNext, ::onError)
    }

    protected fun <T> Single<T>.subscribeOnMain(onSuccess: (T) -> Unit): Disposable {
        return observeOn(AndroidSchedulers.mainThread()).subscribe(onSuccess, ::onError)
    }

    protected fun <T> PublishRelay<T>.subscribeOnMain(onNext: (T) -> Unit): Disposable {
        return observeOn(AndroidSchedulers.mainThread()).subscribe(onNext, ::onError)
    }

    protected fun <T> BehaviorRelay<T>.subscribeOnMain(onNext: (T) -> Unit): Disposable {
        return observeOn(AndroidSchedulers.mainThread()).subscribe(onNext, ::onError)
    }

    private fun onError(e: Throwable) {
        Timber.e(e, "Unexpected error in presenter")
    }

}