package com.kober.yourtickets.presentation.route.list

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.kober.yourtickets.R
import com.kober.yourtickets.domain.dto.route.CityDto
import com.kober.yourtickets.extencions.inflater
import kotlinx.android.synthetic.main.item_city.view.*


class CityAdapterDelegate(
    context: Context,
    @ColorRes private val matchedPatternColorId: Int,
    private val onItemClick: (CityDto) -> Unit
) : AbsListItemAdapterDelegate<CityDto, CityDto, CityViewHolder>() {

    private val inflater = context.inflater
    private val matchedPatternColor = ContextCompat.getColor(context, matchedPatternColorId)

    override fun isForViewType(item: CityDto, items: MutableList<CityDto>, position: Int): Boolean {
        return true // for now it's only one item type in city lists
    }

    public override fun onCreateViewHolder(parent: ViewGroup): CityViewHolder {
        return CityViewHolder(inflater.inflate(R.layout.item_city, parent, false), matchedPatternColor)
    }

    override fun onBindViewHolder(item: CityDto, holder: CityViewHolder, payloads: MutableList<Any>) {
        holder.itemView.setOnClickListener { onItemClick(item) }
        if (payloads.isEmpty()) holder.bind(item)
        else holder.update(item, payloads)
    }
}

class CityViewHolder(itemView: View, private val matchedPatternColor: Int) : RecyclerView.ViewHolder(itemView) {
    fun bind(city: CityDto) {
        itemView.itemCityCode.text = city.code
        val spannable = getMatchInputSpannable(city.searchInputPattern, city.fullName, matchedPatternColor)
        if (spannable != null) itemView.itemCityFullName.setText(spannable, TextView.BufferType.SPANNABLE)
        else itemView.itemCityFullName.text = city.fullName
    }

    fun update(city: CityDto, payloads: MutableList<Any>) {
        val spannable = getMatchInputSpannable(payloads[0] as String, city.fullName, matchedPatternColor)
        if (spannable != null) itemView.itemCityFullName.setText(spannable, TextView.BufferType.SPANNABLE)
        else itemView.itemCityFullName.text = city.fullName
    }

    private fun getMatchInputSpannable(
        inputPattern: String,
        source: String, @ColorInt matchedPatternColor: Int
    ): SpannableString? {
        if (inputPattern.isEmpty()) return null
        val patternStartIndex = source.indexOf(inputPattern, 0, true)
        return if (patternStartIndex != -1) {
            val patternEndIndex = patternStartIndex + inputPattern.length
            SpannableString(source).apply {
                setSpan(
                    ForegroundColorSpan(matchedPatternColor),
                    patternStartIndex,
                    patternEndIndex,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        } else null
    }
}
