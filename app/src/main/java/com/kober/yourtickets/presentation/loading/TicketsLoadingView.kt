package com.kober.yourtickets.presentation.loading

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.kober.yourtickets.domain.dto.route.RouteCitiesDto


interface TicketsLoadingView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun initRoute(routeCities: RouteCitiesDto, loadingProgress: Long, remainingTimeMs: Long)

    @StateStrategyType(SkipStrategy::class)
    fun startPlaneAnimation(loadingIndex: Long)

}