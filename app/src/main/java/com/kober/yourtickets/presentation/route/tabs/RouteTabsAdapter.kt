package com.kober.yourtickets.presentation.route.tabs

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.kober.yourtickets.R
import com.kober.yourtickets.presentation.route.arrival.ChooseArrivalFragment
import com.kober.yourtickets.presentation.route.departure.ChooseDepartureFragment


class RouteTabsAdapter(
    private val context: Context,
    fragmentManager: FragmentManager
) : FragmentPagerAdapter(fragmentManager) {

    override fun getCount() = 2

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ChooseDepartureFragment.newInstance()
            1 -> ChooseArrivalFragment.newInstance()
            else -> throw IllegalArgumentException("Unknown tab")
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.choose_departure_title)
            1 -> context.getString(R.string.choose_arrival_title)
            else -> throw IllegalArgumentException("Unknown tab")
        }
    }
}
