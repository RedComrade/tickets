package com.kober.yourtickets.presentation.route.tabs

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType


interface ChooseRouteView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(message: String)

}