package com.kober.yourtickets.navigation

import com.kober.yourtickets.domain.dto.route.RouteCitiesDto
import com.kober.yourtickets.presentation.loading.TicketsLoadingFragment
import com.kober.yourtickets.presentation.result.TicketsResultFragment
import com.kober.yourtickets.presentation.route.tabs.ChooseRouteFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen


object Screens {

    object ChooseCities : SupportAppScreen() {
        override fun getFragment() = ChooseRouteFragment.newInstance()
    }

    data class TicketsLoading(private val routeCitiesDto: RouteCitiesDto) : SupportAppScreen() {
        override fun getFragment() = TicketsLoadingFragment.newInstance(routeCitiesDto)
    }

    object TicketsResult : SupportAppScreen() {
        override fun getFragment() = TicketsResultFragment.newInstance()
    }

}