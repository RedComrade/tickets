package com.kober.yourtickets

import android.app.Application
import com.kober.yourtickets.di.DI
import com.kober.yourtickets.di.module.AppModule
import timber.log.Timber
import toothpick.Toothpick
import toothpick.configuration.Configuration
import java.util.*


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appCode = UUID.randomUUID().toString()

        initLogger()
        initAppScope()
        initToothpick()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }


    private fun initAppScope() {
        DI.appScope.installModules(AppModule(this))
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }
    }

    companion object {
        lateinit var appCode: String
            private set
    }
}