package com.kober.yourtickets.domain.state.tickets

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.kober.yourtickets.domain.dto.tickets.LoadingInfoDto
import com.kober.yourtickets.domain.dto.route.RouteCitiesDto


class TicketsState(val routeCities: RouteCitiesDto) {

    val ticketsLoadingInfo = BehaviorRelay.create<LoadingInfoDto>()
    val ticketsLoaded = PublishRelay.create<Unit>()
    val ticketsLoadingError = PublishRelay.create<String>()

}