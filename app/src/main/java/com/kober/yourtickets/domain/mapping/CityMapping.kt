package com.kober.yourtickets.domain.mapping

import com.kober.yourtickets.data.server.entity.CitiesResult
import com.kober.yourtickets.data.server.entity.City
import com.kober.yourtickets.domain.dto.route.CityDto


fun CitiesResult.toCityDtoList(input: String): List<CityDto> {
    return cities.map { it.toCityDto(input) }
}

fun City.toCityDto(searchPattern: String): CityDto {
    val code = iataCodes.getOrNull(0) ?: ""
    return CityDto(
        id,
        fullName,
        code,
        location.latitude,
        location.longitude,
        searchPattern
    )
}
