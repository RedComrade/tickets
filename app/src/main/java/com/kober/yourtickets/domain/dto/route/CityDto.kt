package com.kober.yourtickets.domain.dto.route

import java.io.Serializable


data class CityDto(
    val id: Int,
    val fullName: String,
    val code: String,
    val latitude: Double,
    val longitude: Double,
    val searchInputPattern: String
) : Serializable {
    fun isSamePlace(another: CityDto): Boolean {
        return this.latitude == another.latitude && this.longitude == another.longitude
    }
}
