package com.kober.yourtickets.domain.common

import androidx.recyclerview.widget.DiffUtil


/**
 * avoid initialisation on main thread, it could be to heavy
 */
abstract class ListUpdate<T>(diffCallback: DiffUtilsCallback<T>) {
    val list = diffCallback.newList
    val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(diffCallback)
}

abstract class DiffUtilsCallback<T>(private val oldList: List<T>, val newList: List<T>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return areItemsTheSame(oldList[oldPosition], newList[newPosition])
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return areContentsTheSame(oldList[oldPosition], newList[newPosition])
    }

    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return getChangePayload(oldList[oldPosition], newList[newPosition])
    }

    abstract fun areItemsTheSame(old: T, new: T): Boolean

    open fun areContentsTheSame(old: T, new: T): Boolean = old == new

    open fun getChangePayload(old: T, new: T): Any? = null
}