package com.kober.yourtickets.domain.dto.route

import com.kober.yourtickets.domain.common.DiffUtilsCallback
import com.kober.yourtickets.domain.common.ListUpdate


class CityListUpdate(oldList: List<CityDto>, newList: List<CityDto>) : ListUpdate<CityDto>(

    object : DiffUtilsCallback<CityDto>(oldList, newList) {
        override fun areItemsTheSame(old: CityDto, new: CityDto) = old.id == new.id

        override fun getChangePayload(old: CityDto, new: CityDto): Any? {
            // suppose that other params are immutable on server
            return if (old.searchInputPattern != new.searchInputPattern) new.searchInputPattern
            else null
        }
    }

)
