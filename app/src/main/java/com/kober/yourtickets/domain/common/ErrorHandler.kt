package com.kober.yourtickets.domain.common

import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.fasterxml.jackson.databind.exc.PropertyBindingException
import com.kober.yourtickets.BuildConfig
import com.kober.yourtickets.R
import com.kober.yourtickets.data.system.ResourceManager
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject


class ErrorHandler @Inject constructor(
    private val resourceManager: ResourceManager
) {

    fun generateUserMessage(error: Throwable): String {
        return when (error) {
            is HttpException -> {
                when (error.code()) {
                    400 -> resourceManager.getString(R.string.bad_request_error)
                    401 -> resourceManager.getString(R.string.unauthorized_error)
                    403 -> resourceManager.getString(R.string.forbidden_error)
                    404 -> resourceManager.getString(R.string.not_found_error)
                    405 -> resourceManager.getString(R.string.method_not_allowed_error)
                    409 -> resourceManager.getString(R.string.conflict_error)
                    422 -> resourceManager.getString(R.string.unprocessable_error)
                    500 -> resourceManager.getString(R.string.server_error)
                    else -> resourceManager.getString(R.string.unknown_error)
                }
            }
            is JsonMappingException -> resourceManager.getString(R.string.parsing_error)
            is PropertyBindingException -> resourceManager.getString(R.string.parsing_error)
            is MismatchedInputException -> resourceManager.getString(R.string.parsing_error)
            is IOException -> resourceManager.getString(R.string.network_error)
            is SocketTimeoutException -> resourceManager.getString(R.string.server_timeout)
            else -> if (BuildConfig.DEBUG) (error.javaClass.simpleName) else resourceManager.getString(R.string.unknown_error)
        }
    }

}