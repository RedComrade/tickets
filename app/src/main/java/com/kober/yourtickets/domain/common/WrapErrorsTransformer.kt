package com.kober.yourtickets.domain.common

import io.reactivex.*


class WrapErrorsTransformer<T> : SingleTransformer<T, ErrorWrapper<T>> {
    override fun apply(upstream: Single<T>): SingleSource<ErrorWrapper<T>> {
        return upstream
            .map { ErrorWrapper(value = it) }
            .onErrorResumeNext { e: Throwable -> Single.just(ErrorWrapper(error = e)) }
    }
}

class ErrorWrapper<out T> {

    private val value: T?
    private val error: Throwable?

    constructor(error: Throwable) {
        this.value = null
        this.error = error
    }

    constructor(value: T) {
        this.value = value
        this.error = null
    }

    fun unwrap(
        onNext: (T) -> Unit,
        onError: (Throwable) -> Unit
    ) = when {
        value != null -> onNext(value)
        error != null -> onError(error)
        else -> throw IllegalStateException("No value no error state!")
    }
}
