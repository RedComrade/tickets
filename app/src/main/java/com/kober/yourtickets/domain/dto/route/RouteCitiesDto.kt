package com.kober.yourtickets.domain.dto.route

import com.kober.yourtickets.domain.dto.route.CityDto
import java.io.Serializable


data class RouteCitiesDto(val departureCity: CityDto, val arrivalCity: CityDto) : Serializable