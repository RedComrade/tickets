package com.kober.yourtickets.domain.repository

import com.kober.yourtickets.data.server.api.HotelLookApi
import com.kober.yourtickets.domain.dto.route.CityDto
import com.kober.yourtickets.domain.mapping.toCityDtoList
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class CityAutocompleteRepository @Inject constructor(
    private val api: HotelLookApi
) {

    fun getCitySuggestions(input: String, language: String): Single<List<CityDto>> {
        return api.autocomplete(input, language)
            .map { it.toCityDtoList(input) }
            .subscribeOn(Schedulers.io())
    }

}