package com.kober.yourtickets.domain.state.route

import com.jakewharton.rxrelay2.BehaviorRelay
import com.kober.yourtickets.domain.dto.route.CityListUpdate


class DepartureCitiesState : CitiesListState {
    override val listUpdate = BehaviorRelay.create<CityListUpdate>()
    override val loading = BehaviorRelay.createDefault<Boolean>(false)
}