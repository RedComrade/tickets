package com.kober.yourtickets.domain.interactor.route

import com.kober.yourtickets.domain.dto.route.CityDto
import com.kober.yourtickets.domain.dto.route.CityListUpdate
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.SingleTransformer
import io.reactivex.schedulers.Schedulers


class CityListUpdateTransformer : SingleTransformer<List<CityDto>, CityListUpdate> {

    private var oldList: List<CityDto> = listOf()

    override fun apply(upstream: Single<List<CityDto>>): SingleSource<CityListUpdate> {
        return upstream
            .observeOn(Schedulers.computation())
            .map { newList ->
                val result = CityListUpdate(oldList, newList)
                oldList = newList
                return@map result
            }
    }
}