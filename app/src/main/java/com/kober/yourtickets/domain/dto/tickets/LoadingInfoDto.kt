package com.kober.yourtickets.domain.dto.tickets


data class LoadingInfoDto(val loadingIndex: Long, val remainingTimeMs: Long)