package com.kober.yourtickets.domain.interactor.tickets

import com.jakewharton.rxrelay2.PublishRelay
import com.kober.yourtickets.domain.common.ErrorHandler
import com.kober.yourtickets.domain.repository.TicketsRepository
import com.kober.yourtickets.domain.state.tickets.TicketsState
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject


class TicketsInteractor @Inject constructor(
    private val ticketsState: TicketsState,
    private val ticketsRepository: TicketsRepository,
    private val errorHandler: ErrorHandler
) {

    fun getTickets(maxLoadingIndex: Long): Disposable {
        return ticketsRepository.loadTickets(maxLoadingIndex)
            .subscribe(
                { ticketsState.ticketsLoadingInfo.accept(it) },
                { handleError(ticketsState.ticketsLoadingError, it) },
                { ticketsState.ticketsLoaded.accept(Unit) }
            )
    }

    private fun handleError(errorRelay: PublishRelay<String>, e: Throwable) {
        Timber.w(e)
        errorRelay.accept(errorHandler.generateUserMessage(e))
    }

}