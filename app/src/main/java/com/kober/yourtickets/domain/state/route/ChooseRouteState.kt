package com.kober.yourtickets.domain.state.route

import com.jakewharton.rxrelay2.PublishRelay
import com.kober.yourtickets.domain.dto.route.CityDto
import com.kober.yourtickets.domain.dto.route.RouteCitiesDto


class ChooseRouteState {

    var chosenDepartureCity: CityDto? = null
    var chosenArrivalCity: CityDto? = null
    val error = PublishRelay.create<String>()

    fun getRouteIfChosen() = getRouteSafe(chosenDepartureCity, chosenArrivalCity)

    private fun getRouteSafe(cityDep: CityDto?, cityArr: CityDto?): RouteCitiesDto? {
        return if (cityDep != null && cityArr != null) RouteCitiesDto(
            cityDep,
            cityArr
        ) else null
    }

}