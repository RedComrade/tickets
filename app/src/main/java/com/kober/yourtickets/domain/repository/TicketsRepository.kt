package com.kober.yourtickets.domain.repository

import com.kober.yourtickets.domain.dto.tickets.LoadingInfoDto
import io.reactivex.Observable
import java.util.concurrent.TimeUnit


const val LOADING_APPROXIMATE_TIME_MS = 20_000L

class TicketsRepository {

    fun loadTickets(maxLoadingIndex: Long): Observable<LoadingInfoDto> {
        val tickInterval = LOADING_APPROXIMATE_TIME_MS / maxLoadingIndex
        return Observable.interval(0, tickInterval, TimeUnit.MILLISECONDS)
            .takeWhile { it != maxLoadingIndex }
            .map { loadingIndex ->
                val timeRemains = LOADING_APPROXIMATE_TIME_MS - loadingIndex * tickInterval
                LoadingInfoDto(loadingIndex, timeRemains)
            }
    }

}