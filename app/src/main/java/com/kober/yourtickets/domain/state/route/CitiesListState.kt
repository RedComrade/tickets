package com.kober.yourtickets.domain.state.route

import com.jakewharton.rxrelay2.BehaviorRelay
import com.kober.yourtickets.domain.dto.route.CityListUpdate


interface CitiesListState {
    val listUpdate: BehaviorRelay<CityListUpdate>
    val loading: BehaviorRelay<Boolean>
}