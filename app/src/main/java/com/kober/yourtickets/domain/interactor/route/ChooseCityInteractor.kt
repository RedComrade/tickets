package com.kober.yourtickets.domain.interactor.route

import com.jakewharton.rxrelay2.PublishRelay
import com.kober.yourtickets.data.system.DeviceProperties
import com.kober.yourtickets.domain.common.ErrorHandler
import com.kober.yourtickets.domain.common.WrapErrorsTransformer
import com.kober.yourtickets.domain.repository.CityAutocompleteRepository
import com.kober.yourtickets.domain.state.route.ChooseRouteState
import com.kober.yourtickets.domain.state.route.CitiesListState
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val DEBOUNCE_DELAY_MILLIS = 200L

class ChooseCityInteractor @Inject constructor(
    private val citiesListState: CitiesListState,
    private val routeState: ChooseRouteState,
    private val deviceProperties: DeviceProperties,
    private val cityRepository: CityAutocompleteRepository,
    private val errorHandler: ErrorHandler
) {

    private val autocompleteRelay = PublishRelay.create<String>()
    private val cityListUpdateTransformer = CityListUpdateTransformer()


    fun requestCitiesSuggestions(input: String) {
        Timber.w("request input")
        autocompleteRelay.accept(input)
    }

    fun observeCitiesSuggestions(): Disposable {
        return autocompleteRelay
            .doOnNext { citiesListState.loading.accept(true) }
            .debounce(DEBOUNCE_DELAY_MILLIS, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .switchMapSingle { input ->
                cityRepository.getCitySuggestions(input, deviceProperties.getSystemLocale())
                    .compose(cityListUpdateTransformer)
                    .compose(WrapErrorsTransformer())
                    .doOnEvent { _, _ -> citiesListState.loading.accept(false) }
            }
            .subscribe { errorWrapper ->
                errorWrapper.unwrap(
                    { citiesListState.listUpdate.accept(it) },
                    { handleError(routeState.error, it) }
                )
            }
    }

    private fun handleError(errorRelay: PublishRelay<String>, e: Throwable) {
        Timber.w(e)
        errorRelay.accept(errorHandler.generateUserMessage(e))
    }

}