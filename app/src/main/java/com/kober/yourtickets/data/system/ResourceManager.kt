package com.kober.yourtickets.data.system

import android.content.Context
import androidx.annotation.StringRes
import javax.inject.Inject


class ResourceManager @Inject constructor(context: Context) {
    private val resources = context.resources

    fun getString(@StringRes stringId: Int, vararg args: Any?): String = when (args.isEmpty()) {
        true -> resources.getString(stringId)
        else -> resources.getString(stringId, args)
    }
}