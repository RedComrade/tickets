package com.kober.yourtickets.data.system

import android.content.res.Resources
import androidx.core.os.ConfigurationCompat
import androidx.core.os.LocaleListCompat


class DeviceProperties {

    fun getSystemLocale(): String {
        val localesList: LocaleListCompat = ConfigurationCompat.getLocales(Resources.getSystem().configuration)
        return try { // not sure it's absolutely safe
            localesList[0].isO3Language
        } catch (e: Throwable) {
            "en"
        }
    }

}