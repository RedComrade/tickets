package com.kober.yourtickets.data.server.api

import com.kober.yourtickets.data.server.entity.CitiesResult
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface HotelLookApi {

    @GET("/autocomplete")
    fun autocomplete(@Query("term") input: String, @Query("lang") language: String): Single<CitiesResult>

}