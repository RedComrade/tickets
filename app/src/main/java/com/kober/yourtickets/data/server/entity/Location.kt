package com.kober.yourtickets.data.server.entity

import com.fasterxml.jackson.annotation.JsonProperty


data class Location(
    @JsonProperty("lat") val latitude: Double,
    @JsonProperty("lon") val longitude: Double
)