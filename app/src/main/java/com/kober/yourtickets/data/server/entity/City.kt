package com.kober.yourtickets.data.server.entity

import com.fasterxml.jackson.annotation.JsonProperty


data class City(
    @JsonProperty("id") val id: Int,
    @JsonProperty("countryId") val countryId: Int,
    @JsonProperty("city") val city: String,
    @JsonProperty("clar") val country: String,
    @JsonProperty("fullname") val fullName: String,
    @JsonProperty("latinCity") val latinCity: String,
    @JsonProperty("countryCode") val countryCode: String,
    @JsonProperty("_score") val score: Int,
    @JsonProperty("iata") val iataCodes: List<String>,
    @JsonProperty("location") val location: Location
)