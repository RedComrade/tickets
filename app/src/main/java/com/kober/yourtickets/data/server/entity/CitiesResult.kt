package com.kober.yourtickets.data.server.entity

import com.fasterxml.jackson.annotation.JsonProperty


data class CitiesResult(
    @JsonProperty("cities") val cities: List<City>
)