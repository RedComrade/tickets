package com.kober.yourtickets.di.module

import com.kober.yourtickets.domain.interactor.route.ChooseCityInteractor
import com.kober.yourtickets.domain.repository.CityAutocompleteRepository
import com.kober.yourtickets.domain.state.route.CitiesListState
import com.kober.yourtickets.domain.state.route.DepartureCitiesState
import toothpick.config.Module


class DepartureModule : Module() {
    init {
        bind(ChooseCityInteractor::class.java).singletonInScope()
        bind(CitiesListState::class.java).toInstance(DepartureCitiesState())
        bind(CityAutocompleteRepository::class.java).singletonInScope()
    }
}