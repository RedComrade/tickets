package com.kober.yourtickets.di.module

import com.kober.yourtickets.domain.interactor.route.ChooseCityInteractor
import com.kober.yourtickets.domain.repository.CityAutocompleteRepository
import com.kober.yourtickets.domain.state.route.ArrivalCitiesState
import com.kober.yourtickets.domain.state.route.CitiesListState
import toothpick.config.Module


class ArrivalModule : Module() {
    init {
        bind(ChooseCityInteractor::class.java).singletonInScope()
        bind(CitiesListState::class.java).toInstance(ArrivalCitiesState())
        bind(CityAutocompleteRepository::class.java).singletonInScope()
    }
}