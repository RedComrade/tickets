package com.kober.yourtickets.di.provider

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import javax.inject.Provider


class JacksonProvider : Provider<ObjectMapper> {
    override fun get(): ObjectMapper = ObjectMapper().apply {
        propertyNamingStrategy = PropertyNamingStrategy.UPPER_CAMEL_CASE
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        registerModule(KotlinModule())
    }
}
