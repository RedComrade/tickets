package com.kober.yourtickets.di.provider

import com.fasterxml.jackson.databind.ObjectMapper
import com.kober.yourtickets.BuildConfig
import com.kober.yourtickets.data.server.api.HotelLookApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Inject
import javax.inject.Provider


class HotelLookApiProvider @Inject constructor(
        private val okHttpClient: OkHttpClient,
        private val objectMapper: ObjectMapper
) : Provider<HotelLookApi> {

    override fun get(): HotelLookApi {
        return Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(okHttpClient)
                .build()
                .create(HotelLookApi::class.java)
    }

}