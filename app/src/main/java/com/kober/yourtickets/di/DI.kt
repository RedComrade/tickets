package com.kober.yourtickets.di

import toothpick.Scope
import toothpick.Toothpick


object DI {

    val appScope: Scope get() = Toothpick.openScope(APP_SCOPE)
    val routeScope: Scope get() = Toothpick.openScopes(APP_SCOPE, CHOOSE_CITIES_SCOPE)
    val departureScope: Scope get() = Toothpick.openScopes(CHOOSE_CITIES_SCOPE, DEPARTURE_SCOPE)
    val arrivalScope: Scope get() = Toothpick.openScopes(CHOOSE_CITIES_SCOPE, ARRIVAL_SCOPE)
    val ticketsLoadingScope: Scope get() = Toothpick.openScopes(CHOOSE_CITIES_SCOPE, TICKETS_LOADING_SCOPE)


    private const val APP_SCOPE = "app_scope"
    private const val CHOOSE_CITIES_SCOPE = "choose_cities_scope"
    private const val DEPARTURE_SCOPE = "departure_scope"
    private const val ARRIVAL_SCOPE = "arrival_scope"
    private const val TICKETS_LOADING_SCOPE = "tickets_loading_scope"

}

fun Scope.close() {
    Toothpick.closeScope(name)
}

fun Scope.inject(obj: Any) {
    Toothpick.inject(obj, this)
}