package com.kober.yourtickets.di.provider

import com.kober.yourtickets.BuildConfig
import com.kober.yourtickets.data.server.interceptor.CurlLoggingInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Provider


class OkHttpClientProvider : Provider<OkHttpClient> {

    private val httpClient: OkHttpClient

    init {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.readTimeout(READ_TIMEOUT_SEC, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addNetworkInterceptor(httpLoggingInterceptor)
            httpClientBuilder.addNetworkInterceptor(CurlLoggingInterceptor())
        }
        httpClient = httpClientBuilder.build()
    }

    override fun get() = httpClient


    companion object {
        const val READ_TIMEOUT_SEC: Long = 15
    }
}
