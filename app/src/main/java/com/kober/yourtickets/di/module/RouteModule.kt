package com.kober.yourtickets.di.module

import com.kober.yourtickets.domain.state.route.ChooseRouteState
import toothpick.config.Module


class RouteModule : Module() {
    init {
        bind(ChooseRouteState::class.java).toInstance(ChooseRouteState())
    }
}
