package com.kober.yourtickets.di.module

import com.kober.yourtickets.domain.dto.route.RouteCitiesDto
import com.kober.yourtickets.domain.interactor.tickets.TicketsInteractor
import com.kober.yourtickets.domain.repository.TicketsRepository
import com.kober.yourtickets.domain.state.tickets.TicketsState
import toothpick.config.Module


class TicketsLoadingModule(routeCities: RouteCitiesDto) : Module() {
    init {
        bind(TicketsInteractor::class.java).singletonInScope()
        bind(TicketsState::class.java).toInstance(TicketsState(routeCities))
        bind(TicketsRepository::class.java).toInstance(TicketsRepository())
    }
}