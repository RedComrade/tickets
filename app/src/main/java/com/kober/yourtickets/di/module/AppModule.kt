package com.kober.yourtickets.di.module

import android.content.Context
import com.fasterxml.jackson.databind.ObjectMapper
import com.kober.yourtickets.data.server.api.HotelLookApi
import com.kober.yourtickets.data.system.DeviceProperties
import com.kober.yourtickets.data.system.ResourceManager
import com.kober.yourtickets.di.provider.HotelLookApiProvider
import com.kober.yourtickets.di.provider.JacksonProvider
import com.kober.yourtickets.di.provider.OkHttpClientProvider
import com.kober.yourtickets.domain.common.ErrorHandler
import okhttp3.OkHttpClient
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module


class AppModule(context: Context) : Module() {
    init {
        //Global
        bind(Context::class.java).toInstance(context)
        bind(ResourceManager::class.java).toInstance(ResourceManager(context))
        bind(DeviceProperties::class.java).toInstance(DeviceProperties())

        //Navigation
        val cicerone = Cicerone.create(Router())
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        // Network
        bind(ErrorHandler::class.java).singletonInScope()
        bind(ResourceManager::class.java).singletonInScope()
        bind(ObjectMapper::class.java).toProviderInstance(JacksonProvider())
        bind(OkHttpClient::class.java).toProviderInstance(OkHttpClientProvider())
        bind(HotelLookApi::class.java).toProvider(HotelLookApiProvider::class.java).singletonInScope()
    }
}