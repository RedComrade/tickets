package com.kober.yourtickets.extencions

import com.google.android.gms.maps.model.LatLng


fun getBearing(p1: LatLng, p2: LatLng, sourceAngle: Float = 0f): Double {
    val lat1 = p1.latitude * Math.PI / 180
    val long1 = p1.longitude * Math.PI / 180
    val lat2 = p2.latitude * Math.PI / 180
    val long2 = p2.longitude * Math.PI / 180
    val dLon = long2 - long1
    val y = Math.sin(dLon) * Math.cos(lat2)
    val x = Math.cos(lat1) * Math.sin(lat2) - (Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon))
    var result = Math.atan2(y, x)
    result = Math.toDegrees(result)
    result = (result + 360) % 360
    return result - sourceAngle
}