package com.kober.yourtickets.extencions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.hannesdorfmann.adapterdelegates4.AbsDelegationAdapter
import com.kober.yourtickets.domain.common.ListUpdate


fun View.visibleOrGone(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

val Context.inflater: LayoutInflater
    get() = LayoutInflater.from(this)

fun <T> AbsDelegationAdapter<List<T>>.updateItems(listUpdate: ListUpdate<T>, dispatch: Boolean = true) {
    items = listUpdate.list
    if (dispatch) listUpdate.diffResult.dispatchUpdatesTo(this)
    else notifyDataSetChanged()
}

fun getSimpleTextWatcher(afterTextChanged: (Editable) -> Unit): TextWatcher {
    return object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(input: Editable) {
            afterTextChanged(input)
        }
    }
}

fun EditText.setTextIgnoreWatcher(newText: SpannableString, textWatcher: TextWatcher) {
    removeTextChangedListener(textWatcher)
    setText(newText)
    addTextChangedListener(textWatcher)
    setSelection(text?.length ?: 0)
}

fun Context.getBitmapForLayout(parentView: View, @LayoutRes layoutResId: Int, customize: (View) -> Unit): Bitmap {
    val view = LayoutInflater.from(this).inflate(layoutResId, null)
    customize(view)

    val specWidth = View.MeasureSpec.makeMeasureSpec(parentView.width, View.MeasureSpec.AT_MOST)
    val specHeight = View.MeasureSpec.makeMeasureSpec(parentView.height, View.MeasureSpec.AT_MOST)
    view.measure(specWidth, specHeight)
    val width = view.measuredWidth
    val height = view.measuredHeight

    view.layout(0, 0, width, height)

    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    view.draw(canvas)
    return bitmap
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun Context.dimen(dpRes: Int): Float {
    return resources.getDimension(dpRes)
}

fun Fragment.snackBar(text: String, duration: Int = Snackbar.LENGTH_LONG, init: Snackbar.() -> Unit = {}): Snackbar {
    return requireNotNull(view).snackBar(text, duration, init)
}

fun View.snackBar(text: String, duration: Int = Snackbar.LENGTH_SHORT, init: Snackbar.() -> Unit = {}): Snackbar {
    val snack = Snackbar.make(this, text, duration)
    snack.init()
    snack.show()
    return snack
}

fun TextView.isEmpty() = text.isNullOrBlank()
