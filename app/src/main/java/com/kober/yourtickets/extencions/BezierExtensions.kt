package com.kober.yourtickets.extencions

import com.google.android.gms.maps.model.LatLng


fun getBezierPoints(p1: LatLng, p4: LatLng, pointCount: Long): List<LatLng> {
    // todo swap p1 and p4 if closest route if from right to left (VVO -> MSY try for test)
    /**
     * set p2 and p3 from center to p1 and p4 with small shift
     * rotate p2p1 and p3p4 vectors to 90 degree out
     */
    val p1x = p1.longitude
    val p1y = p1.latitude

    val p4x = p4.longitude
    val p4y = p4.latitude

    val midX = (p1x + p4x) / 2
    val midY = (p1y + p4y) / 2
    val dX = Math.abs(p1x - p4x)
    val dY = Math.abs(p1y - p4y)
    val shift = maxOf(dX, dY) / 4
    val angle = 90 * Math.PI / 180

    val (shiftedP2x, shiftedP2y) = getShifted(midX, midY, p1x, p1y, shift)
    val (p2x, p2y) = getRotated(midX, midY, shiftedP2x, shiftedP2y, angle)

    val (shiftedP3x, shiftedP3y) = getShifted(midX, midY, p4x, p4y, shift)
    val (p3x, p3y) = getRotated(midX, midY, shiftedP3x, shiftedP3y, angle)

    // P = (1−t)3P1 + 3(1−t)2tP2 +3(1−t)t2P3 + t3P4; for 4 points
    val pointsList = mutableListOf<LatLng>()
    for (i in 0 until pointCount) {
        val t = i.toFloat() / pointCount
        val arcX = ((1 - t) * (1 - t) * (1 - t) * p1x
                + 3 * (1 - t) * (1 - t) * t * p2x
                + 3 * (1 - t) * t * t * p3x
                + t * t * t * p4x)
        val arcY = ((1 - t) * (1 - t) * (1 - t) * p1y
                + 3 * (1 - t) * (1 - t) * t * p2y
                + 3 * (1 - t) * t * t * p3y
                + t * t * t * p4y)
        pointsList.add(LatLng(arcY, arcX))
    }
    return pointsList
}

private fun getRotated(xCenter: Double, yCenter: Double, x: Double, y: Double, angle: Double): Pair<Double, Double> {
    val xRot = xCenter + Math.cos(angle) * (x - xCenter) - Math.sin(angle) * (y - yCenter)
    val yRot = yCenter + Math.sin(angle) * (x - xCenter) + Math.cos(angle) * (y - yCenter)
    return Pair(xRot, yRot)
}

private fun getShifted(x1: Double, y1: Double, x2: Double, y2: Double, dT: Double): Pair<Double, Double> {
    val distance = getDistance(x1, y1, x2, y2)
    val t = dT / distance
    return Pair(
        (1 - t) * x1 + t * x2,
        (1 - t) * y1 + t * y2
    )
}

private fun getDistance(x1: Double, y1: Double, x2: Double, y2: Double): Double {
    return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1))
}